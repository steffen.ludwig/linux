# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
alias root='root -l'

function cl() {
    cd $*  &&
    ls -l --color=auto
}
