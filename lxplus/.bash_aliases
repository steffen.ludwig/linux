# User specific aliases and functions
alias root='root -l'

function cl() {
    cd $*               &&
    ls -l --color=auto
}

function geant4() {
    source /cvmfs/sft.cern.ch/lcg/views/LCG_94/x86_64-centos7-gcc7-opt/setup.sh  &&
    mkdir -p ~/geant4                                                            &&
    cd ~/geant4                                                                  &&
    ll
}
