# Linux
## Files
|                                        | URL                                                                                                                                                          |
|----------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Kubuntu 18.04 installation instruction | [https://gitlab.com/steffen.ludwig/linux/blob/master/kubuntu/18.04/kubuntu.md](https://gitlab.com/steffen.ludwig/linux/blob/master/kubuntu/18.04/kubuntu.md) |
| Kubuntu 18.04 configuration files      | [https://gitlab.com/steffen.ludwig/linux/tree/master/kubuntu/18.04](https://gitlab.com/steffen.ludwig/linux/tree/master/kubuntu/18.04)                       |
| LXPLUS configuration files             | [https://gitlab.com/steffen.ludwig/linux/tree/master/lxplus](https://gitlab.com/steffen.ludwig/linux/tree/master/lxplus)                                     |
