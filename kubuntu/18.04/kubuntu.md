# Kubuntu 18.04
<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

## Table of Contents
-   [Install Kubuntu](#install-kubuntu)
    -   [Install recommended software](#install-recommended-software)
-   [Backup using git](#backup-using-git)

[↑ top]: #table-of-contents
<br/>
<!-- /TOC -->

## Install Kubuntu
-   [Download](https://kubuntu.org/getkubuntu/) the latest Kubuntu image, install it (in a virtual environment) and update the system via Discover or command line.
-   Install git and openssh-server:
    ```bash
    sudo apt install -y git             \
                        openssh-server  &&
    ssh-keygen
    ```

[↑ top][]

### Install recommended software
-   kubuntu-extras:
    ``` bash
    sudo apt install -y kubuntu-restricted-extras
    ```
-   chrome: visit [https://www.google.com/chrome/b/](https://www.google.com/chrome/b/)
-   chromium:
    ```bash
    sudo apt install -y chromium-browser
    ```
-   libreoffice:
    ```bash
    sudo apt install -y libreoffice
    ```
-   clamav:
    ```bash
    sudo apt install -y clamav clamav-freshclam
    ```
-   keepass2:
    ```bash
    sudo apt install -y keepass2 xdotool
    ```
-   thunderbird:
    ```bash
    sudo apt install -y thunderbird
    ```
-   openvpn:
    ```bash
    sudo apt install -y network-manager-openvpn
    ```
-   atom:
    ```bash
    sudo apt install -y snapd         &&
    sudo snap install atom --classic
    ```
    List of atom packages:
    -   Sublime Style Column Selection
    -   highlight-selected
    -   linter
    -   linter-gcc
    -   linter-flake8
    -   linter-markdown
    -   autocomplete
    -   minimap
    -   markdown-toc
    -   markdown-scroll-sync
    -   language-markdown
    -   default-language


-   metastore:
    ```bash
    sudo apt install -y metastore
    ```
-   skype: visit [https://www.skype.com/en/get-skype/](https://www.skype.com/en/get-skype/)
-   docker:
    ```bash
    sudo apt install -y docker
    ```
-   cockpit:
    ```bash
    sudo apt install -y cockpit cockpit-docker
    ```

[↑ top][]

## Backup using git
-   Become root:
    ```bash
    sudo -i
    ```
-   If not yet done:
    ```bash
    ssh-keygen             &&
    cat ~/.ssh/id_rsa.pub
    ```
-   Initialize git:
    ```bash
    git config --global user.name "Steffen Ludwig"                                    &&
    git config --global user.email "gitlab@storinator.de"                             &&
    cd /                                                                              &&
    git init                                                                          &&
    git remote add origin git@gitlab.3141593.intranet:23/steffen/$HOSTNAME.git
    ```
-   Store metadata and show status:
    ```bash
    backup_dir=(
        /etc
        /home
        /root
        /var
    )                              &&
    for i in ${backup_dir[@]}; do
        cd $i
        metastore -s
    done                           &&
    cd /                           &&
    git status
    ```
-   Add, commit and push:
    ```bash
    cd /                               &&
    git add .                          &&
    git commit -m $(date '+%Y-%m-%d')  &&
    git push -u origin master          &&
    exit
    ```

[↑ top][]
