#!/bin/bash


if [[ $EUID -ne 0 ]]; then
    echo "Error: this script must be run as root"
    exit 1
fi


function vpn_connect() {
    ping -c 1 cherenkov.3141593.intranet >& /dev/null
    if [[ $? -ne 0 ]]; then
        sudo -H -u steffen bash -c 'nmcli c up cherenkov.3141593.intranet'
    fi
}

function vpn_disconnect() {
    ping -c 1 cherenkov.3141593.intranet >& /dev/null
    if [[ $? -eq 0 ]]; then
        sudo -H -u steffen bash -c 'nmcli c down cherenkov.3141593.intranet'
    fi
}

function git_backup() {
    if [[ $EUID -ne 0 ]]; then
        echo "Error: this function must be run as root"
        exit 1
    fi


    root_dir=/
    backup_dir=(
        /etc
        /home
        /root
        /var
    )


    for dir in ${backup_dir[@]}; do
        cd $dir
        metastore -s
    done                                                 &&
    cd $root_dir                                         &&
    git add .                                            &&
    git commit -m $(date '+%Y-%m-%d')
}

function git_backup_push() {
    if [[ $EUID -ne 0 ]]; then
        echo "Error: this function must be run as root"
        exit 1
    fi

    ping -c 1 gitlab.3141593.intranet >& /dev/null
    if [[ $? -ne 0 ]]; then
        echo "Error: backup server not reachable"
        exit 1
    fi


    root_dir=/


    cd $root_dir                                       &&
    git push -u origin master
}
