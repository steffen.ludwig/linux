# User specific aliases and functions
alias root='root -l'
alias lxplus='ssh -Y stludwig@lxplus.cern.ch'

function cl() {
    cd $*               &&
    ls -l --color=auto
}

function mount_afs() {
    mkdir -p ~/afs                                                     &&
    sshfs stludwig@lxplus.cern.ch:/afs/cern.ch/user/s/stludwig/ ~/afs
}

function umount_afs() {
    fusermount -u ~/afs
}
